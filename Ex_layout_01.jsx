import React, { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";
import Main from "./Main";
import Navigate from "./Navigate";
import "./styles.css";

export default class Ex_layout_01 extends Component {
  render() {
    return (
      <div>
        <Navigate />
        <Header />
        <Main />
        <Footer />
      </div>
    );
  }
}
